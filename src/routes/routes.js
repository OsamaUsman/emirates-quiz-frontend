import DashboardLayout from "@/views/Layout/DashboardLayout.vue";
import MainLayout from "@/views/Layout/MainLayout.vue";
import AuthLayout from "@/views/Pages/AuthLayout.vue";
// GeneralViews
import NotFound from "@/views/GeneralViews/NotFoundPage.vue";

// Pages
const Main = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Dashboard/Main.vue");

const Dash = () =>
  import(
    /* webpackChunkName: "dashboard" */ "@/views/Dashboard/Dashboard-T.vue"
  );

const MainAdmin = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Dashboard/MainAdmin.vue");

const Login = () =>
  import(/* webpackChunkName: "pages" */ "@/views/Pages/Login.vue");
const Home = () =>
  import(/* webpackChunkName: "pages" */ "@/views/Pages/Home.vue");
const Register = () =>
  import(/* webpackChunkName: "pages" */ "@/views/Pages/Register.vue");
const Lock = () =>
  import(/* webpackChunkName: "pages" */ "@/views/Pages/Lock.vue");

//myApp
const Offers = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Offers.vue");
const PastWinners = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/PastWinners.vue");

const quiz = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Quiz.vue");
const responses = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Responses.vue");
const question = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Question.vue");
const winner = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Winner.vue");

//myApp

const Categories = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Categories.vue");
//myApp
const MailShots = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/MailShots.vue");
//myApp
const Bookings = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Bookings.vue");
//myApp
const Properties = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Properties.vue");
//myApp
const Users = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Users.vue");
//myApp

const payouts = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Payouts.vue");

const amc = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/AMC.vue");

const users = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Users.vue");
const psrequest = () =>
  import(
    /* webpackChunkName: "dashboard" */ "@/views/Heaven/PropertySellingRequest.vue"
  );
const orders = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Orders.vue");

const settings = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Settings.vue");

const Promocodes = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/PromoCodes.vue");

const aboutus = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/AboutUs.vue");

const productss = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Productss.vue");

const Sales = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Sales.vue");

// const //Categories = () =>
//   import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Categories.vue");

const Reports = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Dashboard/Reports.vue");

const Messages = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Dashboard/Messages.vue");

const time = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Time.vue");

let myMenu = {
  path: "/fusion",
  component: MainLayout,
  name: "fusion",
  redirect: "/fusion/dashboard",
  children: [
    {
      path: "dashboard",
      name: "fusiondashboard",
      component: Dash,
    },
    {
      path: "admin",
      name: "fusiondashboard",
      component: MainAdmin,
    },
    {
      path: "promocodes",
      name: "promocodes",
      component: Promocodes,
    },
    {
      path: "quiz",
      name: "quiz",
      component: quiz,
    },
    {
      path: "responses",
      name: "responses",
      component: responses,
    },
    {
      path: "question",
      name: "question",
      component: question,
    },
    {
      path: "winner",
      name: "winner",
      component: winner,
    },
    {
      path: "psrequest",
      name: "psrequest",
      component: psrequest,
    },
    {
      path: "pastWinners",
      name: "pastWinners",
      component: PastWinners,
    },
    {
      path: "aboutus",
      name: "aboutus",
      component: aboutus,
    },
    {
      path: "users",
      name: "users",
      component: Users,
    },
    {
      path: "time",
      name: "time",
      component: time,
    },
    {
      path: "mailshots",
      name: "mailshots",
      component: MailShots,
    },
    {
      path: "categories",
      name: "categories",
      component: Categories,
    },
    {
      path: "bookings",
      name: "bookings",
      component: Bookings,
    },
    {
      path: "properties",
      name: "properties",
      component: Properties,
    },
    {
      path: "payouts",
      name: "payouts",
      component: payouts,
    },
    {
      path: "amc",
      name: "amc",
      component: amc,
    },
    {
      path: "productss",
      name: "productss",
      component: productss,
    },
    {
      path: "sales",
      name: "Sales",
      component: Sales,
    },
    {
      path: "orders",
      name: "orders",
      component: orders,
    },
    {
      path: "settings",
      name: "settings",
      component: settings,
    },
    {
      path: "offers",
      name: "offers",
      component: Offers,
    },
    {
      path: "messages",
      name: "messages",
      component: Messages,
    },
  ],
};

let authPages = {
  path: "/",
  component: AuthLayout,
  name: "Authentication",
  children: [
    {
      path: "/home",
      name: "Home",
      component: Home,
      meta: {
        noBodyBackground: true,
      },
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
    },
    {
      path: "/register",
      name: "Register",
      component: Register,
    },
    {
      path: "/lock",
      name: "Lock",
      component: Lock,
    },
    { path: "*", name: "404", component: NotFound },
  ],
};

const routes = [
  {
    path: "/",
    redirect: "/login",
    name: "Home",
  },
  myMenu,
  authPages,
];

export default routes;
